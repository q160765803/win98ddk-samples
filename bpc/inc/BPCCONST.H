//////////////////////////////////////////////////////////////////////////////\
//
//  Copyright (c) 1990  Microsoft Corporation
//
//  Module Name:
//
//     Connect.h
//
//  Abstract:
//
//     Include file for connection object
//
//  Author:
//
//     Peter Porzuczek
//
//  Environment:
//
//     Architecturally, there is an assumption in this driver that we are
//     on a little endian machine.
//
//  Notes:
//
//     optional-notes
//
//  Revision History:
//
//     10/30/1996    Initial
//
///////////////////////////////////////////////////////////////////////////////

#ifndef _BPC_CONST_
#define _BPC_CONST_


//
//  Typedefs for structure defined other include files
//
typedef struct _BPC_ADAPTER        BPC_ADAPTER,        *PBPC_ADAPTER, *PADAPTER;
typedef struct _TUNER              TUNER,              *PTUNER;
typedef struct _DEVICE             DEVICE,             *PDEVICE;
typedef struct _BPC_LIST           BPC_LIST,           *PBPC_LIST;
typedef struct _BPC_GROUP          BPC_GROUP,          *PBPC_GROUP;
typedef struct _CONNECTION         CONNECTION,         *PCONNECTION;
typedef struct _CONNECTION_LIST    CONNECTION_LIST,    *PCONNECTION_LIST;
typedef struct _BPC_MPG            BPC_MPG,            *PBPC_MPG;
typedef struct _BPC_TUNING_INFO    BPC_TUNING_INFO,    *PBPC_TUNING_INFO;
typedef struct _TUNING_DEVICE_LIST TUNING_DEVICE_LIST, *PTUNING_DEVICE_LIST;

typedef struct _BPC10_ADAPTER_CHARACTERISTICS  BPC10_ADAPTER_CHARACTERISTICS;
typedef struct _BPC10_ADAPTER_CHARACTERISTICS  BPC_ADAPTER_CHARACTERISTICS;
typedef struct _BPC10_ADAPTER_CHARACTERISTICS *PBPC_ADAPTER_CHARACTERISTICS;
//
// Temperary define
//
#define NdisMediumDSS NdisMediumDix
#define DO_NOTHING
#define UNDEFINED   (0xFFFFFFFF)



//  NDIS Version Information
//
#ifdef NDIS40
#define BPC_NDIS_MAJOR_VERSION  4
#else
#define BPC_NDIS_MAJOR_VERSION  3
#endif
#define BPC_NDIS_MINOR_VERSION  0


//
//  Connection priorities
//
#define BPC_HIGHEST_CONNECT_PRIORITY    32
#define BPC_LOWEST_CONNECT_PRIORITY      1

#define BPC_HIGHEST_DISCONNECT_PRIORITY 32
#define BPC_LOWEST_DISCONNECT_PRIORITY   1

//  Configuration definitions.
//
#define BPC_MAX_PORT_RANGES         5
#define BPC_MAX_PORT_INTERRUPTS     1
#define BPC_MAX_TUNING_DEVICES      2
#define BPC_MAX_DATA_DEVICES        10
#define BPC_MAX_SUBSCID_FILTER      10
#define BPC_MAX_FRAMES_PER_DEVICE   2


#define DSS_MIN_BUFFERS             8L
#define DSS_BUFFER_MAX              (LONG) (1024L * 64L)
#define DSS_BUFFER_MIN              (LONG) (1024L * 4L)
#define DSS_BUFFER_POOL_MAX         (LONG) (DSS_BUFFER_MAX * 32L)    // 64K * 32
#define DSS_BUFFER_POOL_MIN         (LONG) (DSS_BUFFER_MIN * 32L)    // 4K  * 32
#define DSS_MPEG_POOL_SIZE          DSS_BUFFER_POOL_MAX
#define DSS_STD_POOL_SIZE           DSS_BUFFER_POOL_MAX
#define DSS_MPEG_BUFFER_SIZE        DSS_BUFFER_MAX
#define DSS_STD_BUFFER_SIZE         DSS_BUFFER_MAX



//
//  Maximum Buffer size
//
#define DATA_BUFFER_SIZE    1024 * 64
#define BPC_MAX_FRAME_SIZE  DATA_BUFFER_SIZE


//
//  PORT Acces macros
//
#define WRITE_PORT(port,offset,data)    NdisRawWritePortUlong((PULONG)(port+offset), data)
#define READ_PORT(port,offset,data)     NdisRawReadPortUlong((PULONG)(port+offset), data)

//
//  DSS Constants
//
#define DSSOK                       0
#define DSSError                    1
#define PROGRAM_GUIDE_SIZE          240*127
#define SEGM_NPKTS_LIST_OFFSET      19


//
//  BPC Miniport adapter states.
//
#define BPCM_STATE_IDLE                 0
#define BPCM_STATE_QUERY_PENDING        1
#define BPCM_STATE_SET_PENDING          2


#endif // _BPC_CONST_

