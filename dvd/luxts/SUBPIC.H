/***************************************************************************

Copyright (c) 1998  LuxSonor Inc.

Module Name:

	subpic.h

Notes:

	THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
	KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
	PURPOSE.

	Copyright (c) 1998  LuxSonor Inc.  All Rights Reserved.


****************************************************************************/

#ifndef __SUBPIC_H_
#define __SUBPIC_H_


#define SPDECFL_BUF_IN_USE 			0x0001
#define SPDECFL_SUBP_ON_DISPLAY 	0x0002
#define SPDECFL_LAST_FRAME			0x0004
#define SPDECFL_SUBP_DECODED		0x0008
#define SPDECFL_NEW_PIC				0x0010
#define SPDECFL_DECODING			0x0020
#define SPDECFL_RESTART				0x0040
#define SPDECFL_DISP_OFF			0x0080
#define SPDECFL_DISP_FORCED			0x0100
#define SPDECFL_USER_DISABLED		0x0200
#define SPDECFL_DISP_LIVE			0x0400
#define SPDECFL_ONE_UNIT			0x0800
#define SPDECFL_USE_STRAIGHT_PAL	0x1000

typedef struct _SP_DECODE_ADDR {
	UCHAR opCode;
	UCHAR wTopAddr[2];
	UCHAR wBottomAddr[2];
} SP_DECODE_ADDR, *PSP_DECODE_ADDR;

#define DAREA_START_UB_MASK	0x3f	// display area upper bit mask
#define DAREA_END_UB_MASK 0x3		// display area end upper bit mask
#define DAREA_LB_MASK 0xf0

#define DAREA_START_UB_SHIFT 0x4
#define DAREA_END_UB_SHIFT 0x8

#define SP_LCTL_TERM 0xFFFFFF0F
#define SP_MAX_INPUT_BUF	53220	// maximum size of the subpicture input buffer

void SPSetState (PHW_STREAM_REQUEST_BLOCK pSrb);
void SPGetState(PHW_STREAM_REQUEST_BLOCK pSrb);

void SPSetProp (PHW_STREAM_REQUEST_BLOCK pSrb);
void SPGetProp (PHW_STREAM_REQUEST_BLOCK pSrb);

BOOL UpdateSPConsts(PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

BOOL DecodeRLE(PHW_DEVICE_EXTENSION pHwDevExt, PVOID pdest, ULONG cStart, PSP_STRM_EX pspstrmex);

void StartSPDecode (PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

BOOL BuildSPFields (PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

BOOL UpdateSPConsts(PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

VOID SPReceiveDataPacket(PHW_STREAM_REQUEST_BLOCK pSrb);

void DumpPacket(PSP_STRM_EX);

void SPEnqueue(PHW_STREAM_REQUEST_BLOCK pSrb, PSP_STRM_EX pspstrmex);

PHW_STREAM_REQUEST_BLOCK SPDequeue(PSP_STRM_EX pspstrmex);

void SubPicIRQ(PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

void SPSchedDecode(PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

BOOL WriteBMP(PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);
void DumpHLI(PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

void WriteSPBuffer(PHW_DEVICE_EXTENSION pHwDevExt, PBYTE pBuf, ULONG cnt);

void WriteSPData(PHW_DEVICE_EXTENSION pHwDevExt, ULONG ulData);

BOOL AllocateSPBufs(PHW_DEVICE_EXTENSION pHwDevExt, PSP_DECODE_CTL pdecctl);

void CallBackError(PHW_STREAM_REQUEST_BLOCK pSrb);
void CleanSPQueue(PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

void SPSetSPEnable(PHW_DEVICE_EXTENSION pHwDevExt);
void SPSetSPDisable(PHW_DEVICE_EXTENSION pHwDevExt);

BOOL SPChooseMap (PHW_DEVICE_EXTENSION pHwDevExt, PSP_DECODE_CTL pdecctl, PUCHAR pcol, UCHAR con );

void AbortSP(PHW_DEVICE_EXTENSION pHwDevExt, PSP_STRM_EX pspstrmex);

#endif // __SUBPIC_H_
