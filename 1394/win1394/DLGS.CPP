/***************************************************************************

Copyright (c) 1997  Microsoft Corporation

Module Name:

	dlgs.cpp

Abstract:

	Application to use with 1394 diagnostic driver. Dialog procs
	for application.

Environment:

	User mode only

Notes:

	THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
	KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
	PURPOSE.

	Copyright (c) 1997 Microsoft Corporation.  All Rights Reserved.


Revision History:

	6/2/97 : created

Author:

	Tom Green

	
****************************************************************************/


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <commdlg.h>
#include <process.h>
#include <stdlib.h>
#include <string.h>
#include "resource.h"
#include "1394diag.h"
#include "win1394.h"
#include "dlgs.h"
#include "genrouts.h"

// this is to keep track off flags and check box translating
typedef struct
{
	ULONG		Flag;
	ULONG		DialogControl;
} FLAG_TRANSLATE;


FLAG_TRANSLATE		notifyTranslate[] = {	NOTIFY_FLAGS_AFTER_READ, IDC_READ_NOTIFICATION,
											NOTIFY_FLAGS_AFTER_WRITE, IDC_WRITE_NOTIFICATION,
											NOTIFY_FLAGS_AFTER_LOCK, IDC_LOCK_NOTIFICATION};

FLAG_TRANSLATE		accessTranslate[] = {	ACCESS_FLAGS_TYPE_READ, IDC_READ_ACCESS,
											ACCESS_FLAGS_TYPE_WRITE, IDC_WRITE_ACCESS,
											ACCESS_FLAGS_TYPE_LOCK, IDC_LOCK_ACCESS};

FLAG_TRANSLATE		transactionType[] = {	LOCK_TRANSACTION_MASK_SWAP, IDC_MASK_SWAP,
											LOCK_TRANSACTION_COMPARE_SWAP, IDC_COMPARE_SWAP,
											LOCK_TRANSACTION_FETCH_ADD, IDC_FETCH_ADD,
											LOCK_TRANSACTION_LITTLE_ADD, IDC_LITTLE_ADD,
											LOCK_TRANSACTION_BOUNDED_ADD, IDC_BOUNDED_ADD,
											LOCK_TRANSACTION_WRAP_ADD, IDC_WRAP_ADD};

ULONG				notifySize = sizeof(notifyTranslate) / sizeof(FLAG_TRANSLATE);
ULONG				accessSize = sizeof(accessTranslate) / sizeof(FLAG_TRANSLATE);
ULONG				transactionTypeSize = sizeof(transactionType) / sizeof(FLAG_TRANSLATE);


/************************************************************************/
/*							AboutDlgProc								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for About Box.	Includes the ever popular animated	*/
/*  WDM window.															*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static INT			currIcon;

	switch(message)
	{
	case WM_INITDIALOG:
		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_ABOUTBOX_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_ABOUTBOX_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hDlg, TRUE);
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // AboutDlgProc


/************************************************************************/
/*							SetSizeDlgProc								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to set the size of a log or table	*/
/*  in the driver. And, if the animated icon wasn't annoying enough		*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
SetSizeDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	BOOL				bTranslated;
	static INT			currIcon;

	switch(message)
	{
	case WM_INITDIALOG:
		// set up static text to describe what resource we are setting and
		// init size to 0
		SetDlgItemText(hDlg, IDC_SIZE_NAME, szSetSizeText);
		SetDlgItemInt(hDlg, IDC_LOG_SIZE, 0, FALSE);

		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			// get the new size from the edit control if they select OK
			SizeToSet = GetDlgItemInt(hDlg, IDC_LOG_SIZE, &bTranslated, FALSE);
			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // SetSizeDlgProc

/************************************************************************/
/*						AllocRangeDlgProc								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to allocate an address range		*/
/*  in the driver. And, if the animated icon wasn't annoying enough		*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
AllocRangeDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static INT			currIcon;
	ULONG				index;
	CHAR				tmpBuff[STRING_SIZE];

	switch(message)
	{
	case WM_INITDIALOG:
		// put offset low and high and buffer length in edit controls
		_ultoa(pAllocAddress->Offset.Off_High, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH,tmpBuff);

		_ultoa(pAllocAddress->Offset.Off_Low, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff);

		_ultoa(pAllocAddress->Length, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_BUFFER_LENGTH, tmpBuff);

		// set up flag check boxes
		for(index = 0; index < accessSize; index++)
			CheckDlgButton(hDlg, accessTranslate[index].DialogControl,
						   pAllocAddress->AccessType & accessTranslate[index].Flag);

		for(index = 0; index < notifySize; index++)
			CheckDlgButton(hDlg, notifyTranslate[index].DialogControl,
						   pAllocAddress->NotificationOptions & notifyTranslate[index].Flag);
		
		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			// get address offset high and low and buffer length
			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH, tmpBuff, STRING_SIZE);
			pAllocAddress->Offset.Off_High = (USHORT) strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff, STRING_SIZE);
			pAllocAddress->Offset.Off_Low = strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_BUFFER_LENGTH, tmpBuff, STRING_SIZE);
			pAllocAddress->Length = strtoul(tmpBuff, NULL, 16);

			// suck out the check boxes

			pAllocAddress->AccessType = 0;
			pAllocAddress->NotificationOptions = 0;

			for(index = 0; index < accessSize; index++)
			{
				if(IsDlgButtonChecked(hDlg, accessTranslate[index].DialogControl))
					pAllocAddress->AccessType |= accessTranslate[index].Flag;
			}

			for(index = 0; index < notifySize; index++)
			{
				if(IsDlgButtonChecked(hDlg, notifyTranslate[index].DialogControl))
					pAllocAddress->NotificationOptions |= notifyTranslate[index].Flag;
			}

			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // AllocRangeDlgProc

/************************************************************************/
/*						SelectDeviceDlgProc								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to select the 1394 device under		*/
/*  test. And, if the animated icon wasn't annoying enough				*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
SelectDeviceDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND			hWndListBox;
	static INT			currIcon;
	ULONG				Node;
	CHAR				tmpBuff[80];
	static INT			index;

	switch(message)
	{
	case WM_INITDIALOG:
		// get a handle to our listbox
		hWndListBox = GetDlgItem(hDlg, IDC_1394_DEVICES);

		// let's plug all of our devices into the list box
		for(Node = 0; Node < numDevices; Node++)
		{
			wsprintf(tmpBuff, "%08X   %s",
					 pDeviceInfo[Node].nodeInfo.ConfigRom.CR_Info,
					 &pDeviceInfo[Node].nodeInfo.LinkName[4]);
			SendMessage(hWndListBox, LB_ADDSTRING, 0, (LPARAM) tmpBuff);
		}

		// highlight a device in list box
		SendMessage(hWndListBox, LB_SETCURSEL,
					(deviceUnderTest == NO_DEVICE) ? 0 : deviceUnderTest, 0L);

    	// save the original device under test...
		index = SendMessage(hWndListBox, LB_GETCURSEL, 0, 0L);

		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 

        if (bScriptInProgress || deviceUnderTest == NO_DEVICE)
            SendMessage(hDlg, WM_COMMAND, IDOK, 0);

		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_1394_DEVICES:
			if(HIWORD(wParam) != LBN_DBLCLK)
				return TRUE;
			// just fall through here, it's the same thing if they
			// double clicked on device
		case IDOK:
			// get the new device under test
			index = SendMessage(hWndListBox, LB_GETCURSEL, 0, 0L);

			// make sure it's a valid index
			if(index != -1)
				deviceUnderTest = index;

			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			// they clicked on candel, so use the one we came in with...
//			deviceUnderTest = NO_DEVICE;
			// make sure it's a valid index
			if(index != -1)
				deviceUnderTest = index;

			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // SelectDeviceDlgProc

/************************************************************************/
/*						SelectDiagnosticDeviceDlgProc					*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to select the 1394 host				*/
/*  that we will toggle the diagnostic switch on.						*/
/*  And, if the animated icon wasn't annoying enough					*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
SelectDiagnosticDeviceDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND			hWndListBox;
	static INT			currIcon;
	int					index;
	HANDLE				hDev;

	switch(message)
	{
	case WM_INITDIALOG:
		// get a handle to our listbox
		hWndListBox = GetDlgItem(hDlg, IDC_DIAGNOSTIC_DEVICES);

		// let's find all of the host controllers
		for(index = 0; index < 10; index++)
		{
			pDiagnosticDeviceName[11] = '0' + index;

			// try to open it
			hDev = CreateFile(pDiagnosticDeviceName,
							  GENERIC_WRITE | GENERIC_READ,
							  FILE_SHARE_WRITE | FILE_SHARE_READ,
							  NULL,
							  OPEN_EXISTING,
							  0,
							  NULL);

			if(hDev != INVALID_HANDLE_VALUE)
			{
				// add name of device to list box and try again
				SendMessage(hWndListBox, LB_ADDSTRING, 0, (LPARAM) &pDiagnosticDeviceName[4]);
				CloseHandle(hDev);
			}
		}

		// highlight a device in list box
		SendMessage(hWndListBox, LB_SETCURSEL, 0, 0L);

		// setup text for dialog box
		SetDlgItemText(hDlg, IDC_DIAGNOSTIC_DEVICE_TEXT, pDiagnosticText);

		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_DIAGNOSTIC_DEVICES:
			if(HIWORD(wParam) != LBN_DBLCLK)
				return TRUE;
			// just fall through here, it's the same thing if they
			// double clicked on device
		case IDOK:
			// get the device to open
			index = SendMessage(hWndListBox, LB_GETCURSEL, 0, 0L);

			// make sure it's a valid index
			if(index != -1)
			{
				index = SendMessage(hWndListBox, LB_GETCURSEL, 0, 0L);
				//
				// now copy the text back into the device name,
				// this way we can handle non-sequential
				// port devices (not sure if that can happen)
				SendMessage(hWndListBox, LB_GETTEXT, index,
							(LPARAM) &pDiagnosticDeviceName[4]);
			}
			// Hmmm, index thing failed, so reset to 1394bus0
			else
				pDiagnosticDeviceName[11] = '0';

			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			// they clicked on candel, so reset device under test
			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // SelectDiagnosticDeviceDlgProc


/************************************************************************/
/*						WriteAddressDlgProc								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to write an address range			*/
/*  in the driver. And, if the animated icon wasn't annoying enough		*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
WriteAddressDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static INT			currIcon;
	CHAR				tmpBuff[STRING_SIZE];

	switch(message)
	{
	case WM_INITDIALOG:
		// put offset low and high and buffer length in edit controls
		_ultoa(pReadWriteRequest->Offset.Off_High, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH,tmpBuff);

		_ultoa(pReadWriteRequest->Offset.Off_Low, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff);

		_ultoa(pReadWriteRequest->Value, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_VALUE, tmpBuff);

		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			// get address offset high and low and buffer length
			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH, tmpBuff, STRING_SIZE);
			pReadWriteRequest->Offset.Off_High = (USHORT) strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff, STRING_SIZE);
			pReadWriteRequest->Offset.Off_Low = strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_VALUE, tmpBuff, STRING_SIZE);
			pReadWriteRequest->Value = strtoul(tmpBuff, NULL, 16);

			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // WriteAddressDlgProc

/************************************************************************/
/*						ReadAddressDlgProc								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to read an address range			*/
/*  in the driver. And, if the animated icon wasn't annoying enough		*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
ReadAddressDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static INT			currIcon;
	CHAR				tmpBuff[STRING_SIZE];

	switch(message)
	{
	case WM_INITDIALOG:
		// put offset low and high and buffer length in edit controls
		_ultoa(pReadWriteRequest->Offset.Off_High, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH,tmpBuff);

		_ultoa(pReadWriteRequest->Offset.Off_Low, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff);

		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			// get address offset high and low and buffer length
			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH, tmpBuff, STRING_SIZE);
			pReadWriteRequest->Offset.Off_High = (USHORT) strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff, STRING_SIZE);
			pReadWriteRequest->Offset.Off_Low = strtoul(tmpBuff, NULL, 16);

			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // ReadAddressDlgProc

/************************************************************************/
/*						LockAddressDlgProc								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to lock an address range			*/
/*  in the driver. And, if the animated icon wasn't annoying enough		*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
LockAddressDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static INT			currIcon;
	ULONG				index;
	CHAR				tmpBuff[STRING_SIZE];

	switch(message)
	{
	case WM_INITDIALOG:
		// put offset low and high and buffer length in edit controls
		_ultoa(pLockAddress->Offset.Off_High, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH,tmpBuff);

		_ultoa(pLockAddress->Offset.Off_Low, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff);

		_ultoa(pLockAddress->Length, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_BUFFER_LENGTH, tmpBuff);

		_ultoa(pLockAddress->Argument[0], tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_LOCK_ARGUMENT,tmpBuff);

		_ultoa(pLockAddress->DataValue[0], tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_LOCK_DATA,tmpBuff);

		_ultoa(pLockAddress->Argument[1], tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_LOCK_ARGUMENT2,tmpBuff);

		_ultoa(pLockAddress->DataValue[1], tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_LOCK_DATA2,tmpBuff);

        pLockAddress->Add64Bit = FALSE;

		// check the proper radio button
		for(index = 0; index < transactionTypeSize; index++)
		{
			if(transactionType[index].Flag == pLockAddress->TransactionType)
			{
				SendDlgItemMessage(hDlg, transactionType[index].DialogControl,
								   BM_SETCHECK, 1, 0L);
				break;
			}
		}

		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			// get address offset high and low and buffer length
			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH, tmpBuff, STRING_SIZE);
			pLockAddress->Offset.Off_High = (USHORT) strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff, STRING_SIZE);
			pLockAddress->Offset.Off_Low = strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_BUFFER_LENGTH, tmpBuff, STRING_SIZE);
			pLockAddress->Length = strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_LOCK_ARGUMENT, tmpBuff, STRING_SIZE);
			pLockAddress->Argument[0] = strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_LOCK_DATA, tmpBuff, STRING_SIZE);
			pLockAddress->DataValue[0] = strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_LOCK_ARGUMENT2, tmpBuff, STRING_SIZE);
			pLockAddress->Argument[1] = strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_LOCK_DATA2, tmpBuff, STRING_SIZE);
			pLockAddress->DataValue[1] = strtoul(tmpBuff, NULL, 16);

			if(IsDlgButtonChecked(hDlg, IDC_64_BIT_ADD))
			    pLockAddress->Add64Bit = TRUE;
			else
			    pLockAddress->Add64Bit = FALSE;

			// find checked radio button
			for(index = 0; index < transactionTypeSize; index++)
			{
				if(SendDlgItemMessage(hDlg, transactionType[index].DialogControl,
								   BM_GETCHECK, 0, 0L))
				{
					pLockAddress->TransactionType = transactionType[index].Flag;
					break;
				}
			}

			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // LockAddressDlgProc

/************************************************************************/
/*						WriteAddressDlgProc								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to do a loopback test				*/
/*  in the driver. And, if the animated icon wasn't annoying enough		*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
LoopbackDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static INT			currIcon;
	CHAR				tmpBuff[STRING_SIZE];
	static ULONG		CheckBox[]   = { IDC_READ_WRITE, IDC_WRITE_ONLY, IDC_READ_ONLY };
	static INT			CheckBoxSize = sizeof(CheckBox) / sizeof(ULONG);
	INT					index;

	switch(message)
	{
	case WM_INITDIALOG:
		// put offset low and high and buffer length in edit controls
		_ultoa(pReadWriteRequest->Offset.Off_High, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH,tmpBuff);

		_ultoa(pReadWriteRequest->Offset.Off_Low, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff);

		_ultoa(pReadWriteRequest->Length, tmpBuff, 16);
		SetDlgItemText(hDlg, IDC_BUFFER_LENGTH, tmpBuff);

		// setup read write and randomize check boxes
		for(index = 0; index < CheckBoxSize; index++)
		{
			if(CheckBox[index] == pReadWriteRequest->ReadWrite)
			{
				SendDlgItemMessage(hDlg, CheckBox[index], BM_SETCHECK, 1, 0L);
			}
		}

		CheckDlgButton(hDlg, IDC_RANDOM_BUFFER_SIZE, pReadWriteRequest->RandomizeBufferSize);

		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			// get address offset high and low and buffer length
			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_HIGH, tmpBuff, STRING_SIZE);
			pReadWriteRequest->Offset.Off_High = (USHORT) strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_ADDRESS_OFFSET_LOW, tmpBuff, STRING_SIZE);
			pReadWriteRequest->Offset.Off_Low = strtoul(tmpBuff, NULL, 16);

			GetDlgItemText(hDlg, IDC_BUFFER_LENGTH, tmpBuff, STRING_SIZE);
			pReadWriteRequest->Length = strtoul(tmpBuff, NULL, 16);

			// get write only and random buffer size check boxes
			for(index = 0; index < CheckBoxSize; index++)
			{
				if(IsDlgButtonChecked(hDlg, CheckBox[index]))
					pReadWriteRequest->ReadWrite = CheckBox[index];
			}

			pReadWriteRequest->RandomizeBufferSize = IsDlgButtonChecked(hDlg, IDC_RANDOM_BUFFER_SIZE);

			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // LoopbackDlgProc

/************************************************************************/
/*						LocalHostInfoDlgProc							*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Callback routine for Dialog box to get local host info				*/
/*  in the driver. And, if the animated icon wasn't annoying enough		*/
/*  in the AboutBox, it makes a reappearance here.						*/
/*																		*/
/************************************************************************/
BOOL CALLBACK
LocalHostInfoDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static INT			currIcon;

	switch(message)
	{
	case WM_INITDIALOG:
		// check the host ID radio button
		SendDlgItemMessage(hDlg, IDC_HOST_UNIQUE_ID, BM_SETCHECK, 1, 0L);

		// kick off a timer, 8 times a second to animate icon
		SetTimer(hDlg, DLG_TIMER_ID, 125, NULL);

		// init for the current icon
		currIcon = 0;

		// display initial icon
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_DESTROY:
		// free up resources
		KillTimer(hDlg, DLG_TIMER_ID);
		return TRUE;
	case WM_TIMER:
		// cycle through icons
		currIcon = ++currIcon % NUMBER_OF_ICONS;
		SendDlgItemMessage(hDlg, IDC_SETLOG_ICON, STM_SETIMAGE,
						   (WPARAM) IMAGE_ICON, (LPARAM) hIcon[currIcon]); 
		return TRUE;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			// find out which button is checked
			if(SendDlgItemMessage(hDlg, IDC_HOST_UNIQUE_ID, BM_GETCHECK, 0, 0L))
				*pRequest = GET_HOST_UNIQUE_ID;
			else
				*pRequest = GET_HOST_CAPABILITIES;

			EndDialog(hDlg, TRUE);
			return TRUE;
		case IDCANCEL:
			EndDialog(hDlg, FALSE);
			return TRUE;
		default:
			return TRUE;
		}
		break;
	default:
		break;
	}

	return FALSE;
} // LocalHostInfoDlgProc


