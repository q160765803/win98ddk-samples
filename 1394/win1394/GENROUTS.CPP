/***************************************************************************

Copyright (c) 1997  Microsoft Corporation

Module Name:

	genrouts.cpp

Abstract:

	Application to use with 1394 diagnostic driver. General purpose routine
	used by app.

Environment:

	User mode only

Notes:

	THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
	KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
	PURPOSE.

	Copyright (c) 1997 Microsoft Corporation.  All Rights Reserved.


Revision History:

	6/3/97 : created

Author:

	Tom Green

	
****************************************************************************/


#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <commdlg.h>
#include <process.h>
#include <stdlib.h>
#include <string.h>
#include "resource.h"
#include "1394diag.h"
#include "win1394.h"
#include "dlgs.h"
#include "genrouts.h"


/************************************************************************/
/*							SaveBuffer									*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Saves the edit control to a file									*/
/*																		*/
/************************************************************************/
BOOL
SaveBuffer(HWND hWnd)
{
	static char			szFilter[] =	"Log Files (*.LOG)\0*.log\0"	\
										"Text Files (*.TXT)\0*.txt\0"	\
										"All Files (*.*)\0*.*\0\0";
	OPENFILENAME		ofn;
	BOOL				fileResult;
	CHAR				szFileName[_MAX_PATH] = "";
	HANDLE				hFile;
	CHAR				tmpBuff[STRING_SIZE];

	// init file structure
	ofn.lStructSize			= sizeof(OPENFILENAME);
	ofn.hwndOwner			= hWnd;
	ofn.hInstance			= NULL;
	ofn.lpstrFilter			= szFilter;
	ofn.lpstrCustomFilter	= NULL;
	ofn.nMaxCustFilter		= 0;
	ofn.nFilterIndex		= 0;
	ofn.lpstrFile			= szFileName;
	ofn.nMaxFile			= _MAX_PATH;
	ofn.lpstrFileTitle		= NULL;
	ofn.nMaxFileTitle		= _MAX_FNAME + _MAX_EXT;
	ofn.lpstrInitialDir		= NULL;
	ofn.lpstrTitle			= NULL;
	ofn.Flags				= OFN_OVERWRITEPROMPT;
	ofn.nFileOffset			= 0;
	ofn.nFileExtension		= 0;
	ofn.lpstrDefExt			= "log";
	ofn.lCustData			= 0L;
	ofn.lpfnHook			= NULL;
	ofn.lpTemplateName		= NULL;

	fileResult = GetSaveFileName(&ofn);

	// if we succeeded, then write the file
	if(fileResult)
	{
		hFile = CreateFile(szFileName,
						   GENERIC_WRITE | GENERIC_READ,
						   FILE_SHARE_WRITE | FILE_SHARE_READ,
						   NULL,
						   CREATE_ALWAYS,
						   0,
						   NULL);

		if(hFile == INVALID_HANDLE_VALUE)
		{
			wsprintf(tmpBuff, szFileOpenFailed, szFileName);
			MessageBox(hWnd, tmpBuff, "Error", MB_OK);
			fileResult = FALSE;
		}
		else
		{
			INT			iLength;
			PCHAR		pStr;
			DWORD		dwBytesRet;

			// write the edit control to the file

			// get the length of the edit buffer
			iLength = GetWindowTextLength(hWndEdit);

			// get a buffer to store text in, add 1 to iLength to get
			// terminating 0
			pStr = (PCHAR) GlobalAlloc(GPTR, iLength + 1);

			// suck text out of edit control
			GetWindowText(hWndEdit, pStr, iLength + 1);

			// write it to the file
			WriteFile(hFile, pStr, (DWORD) iLength + 1, &dwBytesRet, NULL);

			// free up resources
			CloseHandle(hFile);
			GlobalFree(pStr);
		}
	}

	return fileResult;
} // SaveBuffer

/************************************************************************/
/*							WriteTextToEditControl						*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  write string to edit control										*/
/*																		*/
/************************************************************************/
VOID
WriteTextToEditControl(HWND hWndEdit, PCHAR str)
{
	INT		iLength;

	// get the end of buffer for edit control
	iLength = GetWindowTextLength(hWndEdit);

	// set current selection to that offset
	SendMessage(hWndEdit, EM_SETSEL, iLength, iLength);

	// add text
	SendMessage(hWndEdit, EM_REPLACESEL, 0, (LPARAM) str);

} // WriteTextToEditControl

/************************************************************************/
/*							OpenDevice									*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Wrapper for CreateFile call to open a symbolic link of a device.	*/
/*  bVerbose turns on error dialog boxes. We need to shut this down		*/
/*  when initting and during WM_DEVICECHANGE messages.					*/
/*																		*/
/************************************************************************/
HANDLE
OpenDevice(HWND hWnd, PSTR szDeviceName, BOOL bVerbose)
{
	HANDLE			hDev;
	CHAR			tmpBuff[STRING_SIZE];
	CHAR			szOpenFailed[STRING_SIZE];
	
	hDev = CreateFile(szDeviceName,
					  GENERIC_WRITE | GENERIC_READ,
					  FILE_SHARE_WRITE | FILE_SHARE_READ,
					  NULL,
					  OPEN_EXISTING,
					  FILE_FLAG_OVERLAPPED,
					  NULL);

	if(hDev == INVALID_HANDLE_VALUE && bVerbose)
	{
		LoadString(hInst, IDS_OPEN_FAILED, szOpenFailed, STRING_SIZE);
		wsprintf(tmpBuff, szOpenFailed, &szDeviceName[4]);
		MessageBox(hWnd, tmpBuff, "Error", MB_OK | MB_ICONERROR);
	}

	return hDev;
} // OpenDevice

/************************************************************************/
/*							DeviceIoctl									*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Wrapper for DeviceIoControl call.									*/
/*																		*/
/************************************************************************/
DWORD
DeviceIoctl(HWND hWnd, HANDLE hDev, PSTR szDeviceName, INT IoctlCode,
			PVOID inBuff, INT inBuffSize,
			PVOID outBuff, INT outBuffSize,
			DWORD *dwBytesRet)
{
	DWORD		dwRet;
	CHAR		tmpBuff[STRING_SIZE];
	CHAR		szDeviceIOFailed[STRING_SIZE];

	// set raw address mode
	dwRet = DeviceIoControl(hDev, 
							IoctlCode,
							inBuff,
							inBuffSize, 
							outBuff,
							outBuffSize, 
							dwBytesRet,
							NULL);
	if(!dwRet)
	{
		LoadString(hInst, IDS_DEVIO_FAILED, szDeviceIOFailed, STRING_SIZE);
		wsprintf(tmpBuff, szDeviceIOFailed, &szDeviceName[4]);
		MessageBox(hWnd, tmpBuff, "Error", MB_OK | MB_ICONERROR);
	}

	return dwRet;
} // DeviceIoctl


typedef struct _STRING_TABLE
{
	INT		Code;
	PSTR	Buffer;
} STRING_TABLE;

// resource strings and associated buffers and codes
STRING_TABLE StringList[] =	{	IDS_OPEN_FAILED, szOpenFailed,
								IDS_FILE_OPEN_FAILED, szFileOpenFailed,
								IDS_DEVIO_FAILED, szDeviceIOFailed,
								IDS_BUFFER_CMP_FAILED, szBufferCmpFailed,
								IDS_NO_DEVICES, szNoDevices,
								IDS_NO_DEVICE_SELECTED, szNoDeviceSelected,
								IDS_PERF_STATS, szPerfStats,
								IDS_SIGNATURE, szSignature,
								IDS_ALLOCADDRESS, szAllocAddress,
								IDS_READ_ADDRESS, szReadAddress,
								IDS_APP_NAME, szAppName,
								IDS_CLASS_NAME, szClassName,
								IDS_MEM_ALLOC_FAILED, szMemAllocFailed,
								IDS_GENERATION_COUNT, szGenerationCount,
								IDS_NOTIFICATIONS, szNotifications,
								IDS_TOGGLE_ON, szToggleOn,
								IDS_TOGGLE_OFF, szToggleOff,
								IDS_UNDER_CONSTRUCTION, szUnderConstruction};

INT		StringListSize = sizeof(StringList) / sizeof(STRING_TABLE);

/************************************************************************/
/*							LoadTextStrings								*/
/************************************************************************/
/*																		*/
/* Routine Description:													*/
/*																		*/
/*  Loads up all of our string resources.								*/
/*																		*/
/************************************************************************/
VOID
LoadTextStrings(HINSTANCE hInstance)
{
	INT		index;

	for(index = 0; index < StringListSize; index++)
		LoadString(hInstance, StringList[index].Code, StringList[index].Buffer,
				   STRING_SIZE);
} // LoadTextStrings

